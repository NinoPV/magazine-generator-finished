# Magazine Generator

Het maken van een PDF / Magazine publisher. Schrijf een systeem wat kleine statische websites genereert op basis van het gegeven voorbeeld. De input is een PDF, deze moet omgezet worden in losse JPG plaatjes (per pagina 1 plaatje). Kijk vervolgens wat de aspect ratio van deze afbeelding is, en zet het stuk code in de website om om deze aspect ratio te accepteren. De output is dan het pakketje van de website, met alle afbeeldingen in het juiste mapje, en het script gedeelte aangepast met de juiste aspect ratio.


Doorontwikkeling: automatisch uploaden van het pakketje via FTP.
