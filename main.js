const electron = require('electron');
const url = require('url');
const path = require('path');
const PDFImage = require("pdf-image").PDFImage;
const fs = require('fs');
const os = require('os');
const createHTML = require('create-html');
const probe = require('probe-image-size');
const archiver = require('archiver');
const FileSaver = require('file-saver');
const EasyFtp = require('easy-ftp');

const{app, BrowserWindow, Menu, ipcMain} = electron;

//SET ENV
process.env.NODE_ENV = 'development';

let mainWindow;
let addWindow;

var complete = false;
var pdfImage;
var filePath;
var dir;
var titleFull;

app.on('ready', function() {
    mainWindow = new BrowserWindow({});

    loadMain();

    mainWindow.on('closed', function(){
        app.quit();
    })

    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    Menu.setApplicationMenu(mainMenu);
});

function loadMain(){
    if(complete == false) {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, "mainWindow.html"),
            protocol: 'file:',
            slashes: true
        }));
    }
    else {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, "completeWindow.html"),
            protocol: 'file:',
            slashes: true
        }));
    }
}

function createAddWindow(){
    addWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: 'Add File'
    });
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
    addWindow.on('close', function(){
       addWindow = null;
    })
}

ipcMain.on('file:add', function(e, file){
    filePath = file;
    complete = true;

    loadMain();

    if(addWindow) {
        addWindow.close();
    }
});

ipcMain.on('site:create', function(e, titleUncut){
    var siteNr = 1;
    var title = titleUncut.strip();
    dir = os.tmpdir() + '/' + title;
    titleFull = title;

    while (fs.existsSync(dir)){
        siteNr++
        dir += siteNr;
    }

    console.log(dir);

    fs.mkdirSync(dir);
    fs.mkdirSync(dir + '/img');

    fs.copyFileSync(filePath, dir + '/img/page.pdf');

    pdfImage = new PDFImage(dir + '/img/page.pdf');

    pdfImage.convertFile().then(function (imagePaths) {
        fs.unlinkSync(dir + '/img/page.pdf');
        createFiles(imagePaths, title, e);
    }).catch(e => {
        console.error(e)
    });

    process.on('uncaughtException', function (err) {
        console.log('Caught exception: ' + err);
    });
    
    //fs.copyFile(imagePath, dir + '/app.min.css', (err) => {
    //    if (err) throw err;
    //});
    
    //app.quit();
});

ipcMain.on('create:zip', function(){
    //Archiver
    console.log('create:zip');
    var index = dir + '/index.html';
    var js = dir + '/app.min.js';
    var css = dir + '/app.min.css';
    var desktop = path.join(os.homedir(), 'Desktop');
    var output = fs.createWriteStream(desktop + '/Website.zip');
    var archive = archiver('zip', {
        zlib: { level: 6 } // Sets the compression level.
    });
    archive.pipe(output);
    archive.append(fs.createReadStream(index), { name: 'index.html' });
    archive.append(fs.createReadStream(js), { name: 'app.min.js' });
    archive.append(fs.createReadStream(css), { name: 'app.min.css' });
    var directories = [dir + '/img']

    for(var i in directories) {
        archive.directory(directories[i], directories[i].replace(dir + '/img', '/img'));
    }
    archive.finalize();
})

String.prototype.strip = function() {
    return this.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
};

ipcMain.on('upload:ftp', function(){
    //FTP
    console.log('upload:ftp');
    console.log(titleFull);
    var ftp = new EasyFtp();
    var config = {
        host: 'ftp.et-id.com',
        port: 21,
        username: 'nino@et-id.com',
        password: 'w2ZKOmQF',
        type : 'ftp'
    };
    ftp.connect(config);
    ftp.mkdir("/" + titleFull, function(err){});
    ftp.cd("/" + titleFull, function(err, path){});
    ftp.upload(dir, "/" + titleFull, function(err){});
})

function createFiles(files, title) {
    var body = '<div id="flipbook" class="sample-flipbook">';
    var pathArray = [];
    var imgWidth = '';
    var imgHeigth = '';

    files.forEach(function(file) {
        const pathSingle = file.split("/");
        const fileName = pathSingle[8];
        pathArray.push(fileName);
    });
    
    for (var i = 0; i < pathArray.length; i++) {
        if (i == 0 || i == 1 || i == (pathArray.length - 1) || i == (pathArray.length - 2)) {
            body += '<div class="hard"><img src="img/' + pathArray[i] + '" /></div>';
        }
        else {
            body += '<div><img src="img/' + pathArray[i] + '" /></div>';
        }
    }

    var input = fs.createReadStream(dir + '/img/' + pathArray[0]);

    probe(input).then(result => {
        imgWidth = result.width;
        imgHeigth = result.height;

        body += '</div><script type="text/javascript">var aspectRatio = '+ imgWidth +' / '+ imgHeigth +'; var height = 0.9; $("#flipbook").turn({ width: Math.floor(window.innerHeight * aspectRatio) * (height * 2), height: window.innerHeight * height,autoCenter: true});</script>';
        //CreateHTML
        var html = createHTML({
            title: title,
            css: 'app.min.css',
            lang: 'nl',
            head: ['<meta charset="utf-8">', '<script src="app.min.js" type="text/javascript"></script>'],
            body: body
        })

        fs.writeFile(dir + '/index.html', html, function (err) {
            if (err) console.log(err)
        });
        fs.copyFile('base/app.min.css', dir + '/app.min.css', (err) => {
            if (err) throw err;
        });
        fs.copyFile('base/app.min.js', dir + '/app.min.js', (err) => {
            if (err) throw err;
        });
        input.destroy();
        //event.sender.send('mainprocess-response', title, dir);

        // fs.copyFileSync(dir + '/Website.zip', desktop, (err) => {
        //     if (err) throw err;
        //     console.log('source was copied to destination');
        // });
    });
}

//Create menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu:[
            {
                label: 'Voeg bestand toe',
                accelerator: process.platform == 'darwin' ? 'Command+A' : 'Ctrl+A',
                click(){
                    if(BrowserWindow.getAllWindows().length < 2){
                        createAddWindow();
                    }
                }
            },
            {
                label: 'Sluit app',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click(){
                    app.quit();
                }
            }
        ]
    }
];

if(process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu:[
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }
        ]
    })
}